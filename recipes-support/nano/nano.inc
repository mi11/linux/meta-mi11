DESCRIPTION = "GNU nano (Nano's ANOther editor, or \
Not ANOther editor) is an enhanced clone of the \
Pico text editor."
HOMEPAGE = "http://www.nano-editor.org/"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=f27defe1e96c2e1ecd4e0c9be8967949"
SECTION = "console/utils"
DEPENDS = "ncurses"
#ncurses-terminfo is not necessary and too long to install (/usr/share/terminfo files)
#RDEPENDS_${PN} = "ncurses-terminfo"
#add another dependency for educational purpose
RDEPENDS_${PN} = "ncurses-tools"

INC_PR = "r4"

PV_MAJOR = "${@d.getVar('PV',1).split('.')[0]}.${@d.getVar('PV',1).split('.')[1]}"

SRC_URI = "http://www.nano-editor.org/dist/v${PV_MAJOR}/nano-${PV}.tar.gz"

inherit autotools gettext

PACKAGECONFIG[tiny] = "--enable-tiny,--enable-all,"
