SECTION = "base"
DESCRIPTION = "xenomai"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://include/COPYING;md5=79ed705ccb9481bf9e7026b99f4e2b0e"

inherit autotools
inherit pkgconfig

SRC_URI = "https://xenomai.org/downloads/xenomai/stable/xenomai-3.1.tar.bz2\
		https://gitlab.com/Xenomai/xenomai-hacker-space/-/commit/9768c8ec898085287c783edeec025f9c5c37a2b7.patch;name=rt_pipe_patch"
SRC_URI[sha256sum] = "6469aa40d0e61aca9342ba39e822b710177734b171e53a4ab9abb97401a2c480"
SRC_URI[rt_pipe_patch.sha256sum] = "570581940e945c412d3f2f8620fa930400b845d74f63339cad9efaee99a7155a"

RDEPENDS_${PN} = "bash"

S = "${WORKDIR}/${PN}-${PV}"

EXTRA_OECONF = "--prefix=/usr --includedir=/usr/include/xenomai --build=i686-pc-linux-gnu --host=arm-poky-linux-gnueabi --with-core=cobalt"

do_install() {
	oe_runmake 'DESTDIR=${D}' 'SUDO=/bin/true' install
	#remove unused files
	rm -rf ${D}/dev
    rm -rf ${D}/usr/demo

    #adapt xeno-config script to sdk path
    #todo: use relocate script in case sdk is not installed in default path
    sed -i "s|\${DESTDIR}|${SDKPATHINSTALL}/sysroots/${TUNE_PKGARCH}${TARGET_VENDOR}-${TARGET_OS}|g" ${D}/usr/bin/xeno-config
    sed -i "s|sysroot=${WORKDIR}/recipe-sysroot|sysroot=${SDKPATHINSTALL}/sysroots/${TUNE_PKGARCH}${TARGET_VENDOR}-${TARGET_OS}|g" ${D}/usr/bin/xeno-config
}

PACKAGES = "${PN}-dbg ${PN}-dev ${PN}-staticdev ${PN}"

# Fixes QA Error - Non -dev package contains symlink .so
FILES_${PN}-dbg += "/usr/bin/regression/posix/.debug/* /usr/bin/regression/native/.debug/* /usr/bin/regression/native+posix/.debug/*"
FILES_${PN}-staticdev += "/usr/lib/*.a"
FILES_${PN}-dev += "/usr/bin/xeno-config /usr/lib/*.so /usr/lib/*.wrappers /usr/lib/dynlist.ld /usr/include/*"
FILES_${PN} += "/usr/bin/* /usr/lib/*.so*"

TARGET_CC_ARCH += "${LDFLAGS}"

