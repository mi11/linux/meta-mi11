IMAGE_INSTALL += "bash stress evtest"

# Include modules in rootfs
IMAGE_INSTALL += " \
	kernel-modules \
    setserial \
	"

SPLASH = "psplash"

IMAGE_FEATURES += "ssh-server-dropbear splash package-management"
