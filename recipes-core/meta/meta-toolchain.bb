SUMMARY = "Meta package for building a installable toolchain"
LICENSE = "MIT"

inherit populate_sdk

TOOLCHAIN_TARGET_TASK_append = " xenomai-dev"
